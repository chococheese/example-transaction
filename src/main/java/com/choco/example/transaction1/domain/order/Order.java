package com.choco.example.transaction1.domain.order;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@ToString
@EqualsAndHashCode(of = "orderId")
@Table(name = "choco.order")
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "orderId")
	private Long orderId;

	@Column(name = "price")
	private long price;

	@Column(name = "cancelPrice")
	private long cancelPrice;

	@Column(name = "createdAt")
	private LocalDateTime createdAt;

	@Column(name = "modifiedAt")
	private LocalDateTime modifiedAt;

	public Order() {
	}

	@Builder
	public Order(long price, long cancelPrice, LocalDateTime createdAt, LocalDateTime modifiedAt) {
		this.price = price;
		this.cancelPrice = cancelPrice;
		this.createdAt = createdAt;
		this.modifiedAt = modifiedAt;
	}
}
