package com.choco.example.transaction1.domain.order;

import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Long> {
	Order findByOrderId(Long orderId);
}
