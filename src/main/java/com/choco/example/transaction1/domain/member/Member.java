package com.choco.example.transaction1.domain.member;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@ToString
@EqualsAndHashCode(of = "memberSrl")
@Table(name = "choco.member")
public class Member {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "memberSrl")
	private Long memberSrl;

	@Column(name = "name")
	private String name;

	@Column(name = "age")
	private Integer age;

	@Column(name = "gender")
	private Gender gender;

	@Lob
	@Column(name = "description")
	private String description;

	@Column(name = "createdAt")
	private LocalDateTime createdAt;

	@Column(name = "modifiedAt")
	private LocalDateTime modifiedAt;

	public Member() {
	}

	@Builder
	public Member(String name, Integer age, Gender gender, String description, LocalDateTime createdAt, LocalDateTime modifiedAt) {
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.description = description;
		this.createdAt = createdAt;
		this.modifiedAt = modifiedAt;
	}
}
