package com.choco.example.transaction1.domain.member;

import org.springframework.data.repository.CrudRepository;

public interface MemberRepository extends CrudRepository<Member, Long> {
	Member findByMemberSrl(Long memberSrl);
}
