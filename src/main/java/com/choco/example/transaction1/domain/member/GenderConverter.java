package com.choco.example.transaction1.domain.member;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class GenderConverter implements AttributeConverter<Gender, String> {
	@Override
	public String convertToDatabaseColumn(Gender attribute) {
		return attribute.getCode();
	}

	@Override
	public Gender convertToEntityAttribute(String dbData) {
		return Gender.from(dbData);
	}
}
