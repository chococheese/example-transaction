package com.choco.example.transaction1.domain.member;

import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public enum Gender {
	FEMALE("F"),
	MALE("M");

	private static final Map<String, Gender> CODE_BY_GENDER;

	static {
		CODE_BY_GENDER = new HashMap<>();
		Arrays.stream(Gender.values()).forEach(gender -> CODE_BY_GENDER.put(gender.getCode(), gender));
	}

	@Getter
	private String code;

	Gender(String code) {
		this.code = code;
	}

	public static Gender from(String code) {
		return CODE_BY_GENDER.get(code);
	}
}
