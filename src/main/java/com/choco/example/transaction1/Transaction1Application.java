package com.choco.example.transaction1;

import com.choco.example.transaction1.test.ChocoTestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class Transaction1Application implements CommandLineRunner {

	@Autowired
	private ChocoTestService chocoTestService;

	public static void main(String[] args) {
		SpringApplication.run(Transaction1Application.class, args);
	}

	@Override
	public void run(String... args) {
		log.info("Transaction1Application run start.");

		chocoTestService.test();

		log.info("Transaction1Application run end.");
	}
}
