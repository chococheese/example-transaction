package com.choco.example.transaction1.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ChocoApplicationConfig {
	@Bean
	@Qualifier("chocoRestTemplate")
	public RestTemplate chocoRestTemplate() {
		return new RestTemplate();
	}
}
