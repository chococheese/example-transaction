package com.choco.example.transaction1.test;

import com.choco.example.transaction1.external.HtmlPageAdapter;
import com.choco.example.transaction1.service.MemberUpdateService;
import com.choco.example.transaction1.service.OrderUpdateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class ChocoTestService {
	@Autowired
	private OrderUpdateService orderUpdateService;
	@Autowired
	private MemberUpdateService memberUpdateService;
	@Autowired
	private HtmlPageAdapter htmlPageAdapter;

	@Transactional
	public void test() {
		orderUpdateService.update(1L);

		memberUpdateService.update(1L);

		String naverHtml = htmlPageAdapter.getPage("https://www.naver.com");
		log.info("naverHtml: {}", naverHtml.substring(0, 10));
	}
}
