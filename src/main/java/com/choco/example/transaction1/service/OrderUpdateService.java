package com.choco.example.transaction1.service;

import com.choco.example.transaction1.domain.order.Order;
import com.choco.example.transaction1.domain.order.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Slf4j
@Service
public class OrderUpdateService {
	@Autowired
	private OrderRepository orderRepository;

	@Transactional
	public void update(Long orderId) {
		LocalDateTime now = LocalDateTime.now();

		Order order = orderRepository.findByOrderId(orderId);
		LocalDateTime beforeModifiedAt = order.getModifiedAt();

		order.setModifiedAt(now);
		log.info("Order {} modified. modifiedAt: {} -> {}", orderId, beforeModifiedAt, now);
	}
}
