package com.choco.example.transaction1.service;

import com.choco.example.transaction1.domain.member.Member;
import com.choco.example.transaction1.domain.member.MemberRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Slf4j
@Service
public class MemberUpdateService {
	@Autowired
	private MemberRepository memberRepository;

	@Transactional
	public void update(Long memberSrl) {
		LocalDateTime now = LocalDateTime.now();

		Member member = memberRepository.findByMemberSrl(memberSrl);
		LocalDateTime beforeModifiedAt = member.getModifiedAt();

		member.setModifiedAt(now);
		log.info("Member {} modified. modifiedAt: {} -> {}", memberSrl, beforeModifiedAt, now);
	}
}
