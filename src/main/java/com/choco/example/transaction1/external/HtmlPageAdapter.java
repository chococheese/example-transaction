package com.choco.example.transaction1.external;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class HtmlPageAdapter {
	@Autowired
	@Qualifier("chocoRestTemplate")
	private RestTemplate chocoRestTemplate;

	public String getPage(String url) {
		return chocoRestTemplate.getForObject(url, String.class);
	}
}
