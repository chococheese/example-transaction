USE choco;

CREATE TABLE choco.member (
	`memberSrl` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'member 시리얼 넘버',
	`name` VARCHAR(50) NOT NULL COMMENT '이름',
	`age` INT COMMENT '나이',
	`gender` VARCHAR(15) COMMENT '성별. (FEMALE, MALE)',
	`description` TEXT COMMENT '특이사항',
	`createdAt` DATETIME(6) NOT NULL COMMENT '가입일자',
	`modifiedAt` DATETIME(6) NOT NULL COMMENT 'member 정보 변경 일자',
	PRIMARY KEY (`memberSrl`),
	KEY `idx_name` (`name`),
	KEY `idx_modifiedAt` (`modifiedAt`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='member 테이블';

CREATE TABLE choco.order (
  `orderId` BIGINT NOT NULL AUTO_INCREMENT COMMENT '주문번호',
  `price` BIGINT NOT NULL COMMENT '주문금액',
  `cancelPrice` BIGINT NOT NULL COMMENT '주문취소금액',
  `createdAt` DATETIME(6) NOT NULL COMMENT '주문시각',
	`modifiedAt` DATETIME(6) NOT NULL COMMENT '주문 정보 변경 일자',
	PRIMARY KEY (`orderId`),
	KEY `idx_modifiedAt` (`modifiedAt`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='order 테이블';

insert into choco.member (`name`, `age`, `gender`, `description`, `createdAt`, `modifiedAt`) values
('choco', 30, 'M', 'genius', '2018-08-06T23:50:32.000', '2018-08-06T23:50:32.000'),
('cheese', 19, 'F', 'poor', '2018-08-06T23:50:35.000', '2018-08-06T23:50:35.000');

insert into choco.order (`price`, `cancelPrice`, `createdAt`, `modifiedAt`) values
(20000, 5000, '2018-08-06T23:50:42.000', '2018-08-06T23:50:42.000');

select * from choco.order;
select * from choco.member;


